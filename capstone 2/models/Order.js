const mongoose = require("mongoose");
const User = require("../models/User.js");


const orderSchema = new mongoose.Schema({
	userId : {
		type : String,
		required : [true, "USER ID is required"]
	},
	products : [
		{
			productId : {
				type : String,
				required : [false, "PRODUCT ID is required"]
			}
		},
		{
			quantity : {
				type : Number,
				required : [false, "NUMBER is required"]
			}
		}
	],
	totalAmount : {
		type : Number,
		required : [false, "AMOUNT is required"]
	},
	purchasedOn : {
		type : Date,
		default : new Date()
	}
	
		
	
	
})




module.exports = mongoose.model("Order", orderSchema);