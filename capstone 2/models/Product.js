const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "NAME is required!"]
	},
	description : {
		type : String,
		required : [true, "DESCRIPTION is required!"]
	},
	price : {
		type : Number,
		required : [true, "PRICE is required!"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	}

	
})




module.exports = mongoose.model("Product", productSchema);