const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email : {
		type : String,
		required : [true, "EMAIL is required!"]
	},
	password : {
		type : String,
		required : [true, "PASSWORD is required!"]
	},
	isAdmin : {
		type : Boolean,
		default : false,
	},
	orders : [
		{
			productId : {
				type : String,
				default : "none"
			},
			createdOn : {
				type : Date,
				default : new Date()
			}
		}
	]
	
})



module.exports = mongoose.model("User", userSchema);