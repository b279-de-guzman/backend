const Order = require("../models/Order.js");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product.js");
const User = require("../models/User.js");


// Create order (Non-Admin)
module.exports.createOrder = async (data) => {
	
	const { user, order } = data;

	if (user.isAdmin){
		return "You are using an ADMIN account, please change your account"
	}

	const product = await Product.findById(order._id);
	console.log(product);
	if(product) {
		let totalAmount = product.price * order.quantity;
		let newOrder = new Order({
			userId : user.id,
			products : {
				productId : product._id,
				quantity : order.quantity
			},
			totalAmount,
			purchasedOn : new Date()
		})

		await newOrder.save();
		console.log(product);
		return "Purchased Successfully";
	} else {
		return "Product not found!"
	}
}

	 
	

// TEST ORDER

module.exports.testOrder = (reqBody, authId) => {
	
	let testOrder = new Order ({
		userId : authId,
		totalAmount : reqBody.totalAmount
	})


	return testOrder.save().then((order, error) => {
	if(error){
		return error;
	} else {
		return "Order successfully created!";
	}
})

}
