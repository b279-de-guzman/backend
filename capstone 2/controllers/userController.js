const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product.js")
const Order = require("../models/Order.js")


// Register a user
module.exports.registerUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if(result.email == reqBody.email) {
			return "User already exists! Choose another one."
		} else {
			let newUser = new User ({
			email : reqBody.email,
			password : bcrypt.hashSync(reqBody.password, 10)
		})


		return newUser.save().then((user, error) => {
		if(error){
			return error;
		} else {
			return "User successfully registered!";
		}
		})
	}
})
}
	

// Login / Authenticate a user
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
};


// Retrieve all users
module.exports.getAllUsers = (reqBody) => {
	return User.find({}).then(result => {
		return result;
	})
}


// Retrieve user details
module.exports.getUser = (reqParams) => {
	return User.findById(reqParams.id).then(result => {
		return result;
	})
}