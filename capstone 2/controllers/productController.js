const Product = require("../models/Product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User.js");
const Order = require("../models/Order.js");



// Create a product (Admin)
module.exports.createProduct = (data) => {
	
	if(data.isAdmin) {
		let newProduct = new Product({
			name: data.name,
			description: data.description,
			price: data.price
		})

		return newProduct.save().then((product, error) => {
			if(error){
				return error;
			}
			return product;
		})
	} 

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
}


// Retrieve all products (Admin)
module.exports.getAllProducts = (reqBody, isAdmin) => {
	if(isAdmin){
		return Product.find({}).then(result => {
		return result;
	})
	}
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
	
}


// Retrieve all active products
module.exports.getActiveProducts = (reqBody, isAdmin) => {
	if(isAdmin){
		return Product.find({isActive : true}).then(result => {
		return result;
	})
	}
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
	
} 
	


// Retrieve single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.id).then(result => {
		return result;
	})
}


// Update product information (Admin)
module.exports.updateProduct = (reqParams, reqBody, isAdmin) => {

	if(isAdmin){
		let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
		}

		return Product.findByIdAndUpdate(reqParams.id, updatedProduct).then((product, error) => {
			if(error){
				return false;
			} else {
				return "Product Updated";
			}
		})
	} 

	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
	
}


// Archive a product (Admin)
module.exports.archiveProduct = (reqParams, reqBody, isAdmin) => {

	if(isAdmin){
		let archivedProduct = {
			isActive : reqBody.isActive
		}
		return Product.findByIdAndUpdate(reqParams.id, archivedProduct).then((product, error) => {
			if(error){
				return false;
			} else {
				return "Product is archived!";
			}
		})
	}

	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
}