const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js")
const auth = require("../auth.js")


// Creating a product
router.post("/create", auth.verify, (req, res) => {
	let data = {
		name : req.body.name,
		description : req.body.description,
		price : req.body.price,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	productController.createProduct(data).then(resultFromController => 
		res.send(resultFromController));
})


// Retrieve all products
router.get("/all", (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.getAllProducts(req.body, isAdmin).then(resultFromController => 
		res.send(resultFromController));
})


// Retrieve all active products
router.get("/", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.getActiveProducts(req.body, isAdmin).then(resultFromController => 
		res.send(resultFromController));
})


// Retrieve single product
router.get("/:id", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => 
		res.send(resultFromController));
})


// Update product information (Admin)
router.put("/:id", (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.updateProduct(req.params, req.body, isAdmin).then(resultFromController => 
		res.send(resultFromController));
})


// Archive a product (Admin)
router.put("/:id/archive", (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.archiveProduct(req.params, req.body, isAdmin).then(resultFromController => 
		res.send(resultFromController));
})








// Allows us to export the "router" object that will be accessed in our index.js file
module.exports = router;