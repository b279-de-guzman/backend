const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js")
const auth = require("../auth.js")


// Registering a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => 
		res.send(resultFromController));
})

// Login / Authenticate a user
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => 
		res.send(resultFromController));
})

// Retrieve all users
router.get("/all", (req, res) => {
	userController.getAllUsers(req.body).then(resultFromController => 
		res.send(resultFromController));
})


// Retrieve user details
router.get("/:id", (req, res) => {
	userController.getUser(req.params).then(resultFromController => 
		res.send(resultFromController));
})



















// Allows us to export the "router" object that will be accessed in our index.js file
module.exports = router;