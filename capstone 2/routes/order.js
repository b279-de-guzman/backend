const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController.js")
const auth = require("../auth.js")


// Creating an order
router.post("/checkout", auth.verify, (req, res) => {
	const data = {
		user : auth.decode(req.headers.authorization),
		order : req.body,
	};

	orderController.createOrder(data).then(resultFromController => 
		res.send(resultFromController));
})

	

router.post("/testorder", auth.verify, (req, res) => {
	let authId = auth.decode(req.headers.authorization).id
	orderController.testOrder(req.body, authId).then(resultFromController => 
		res.send(resultFromController));
})


















// Allows us to export the "router" object that will be accessed in our index.js file
module.exports = router;