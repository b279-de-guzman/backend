const  mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName : {
        type : String,
        required : [true, "FIRST NAME is required!"]
    },
    lastName : {
        type : String,
        required : [true, "LAST NAME is required!"]
    },
    email : {
        type : String,
        required : [true, "EMAIL is required!"]
    },
    password : {
        type : String,
        required : [true, "PASSWORD is required!"]
    },
    isAdmin : {
        type : Boolean,
        default : false
    },
    mobileNo : {
        type : String,
        required : [true, "MOBILE NO is required!"]
    },
    orderedProduct : [
        {
            product :[
                {
                    productId : {
                        type : String,
                        
                    },
                    productName : {
                        type : String,
                        
                    },
                    price : {
                        type : Number
                    },
                    quantity: {
                        type : Number,
                        
                    }
                }
            ],
            subTotal : {
                type : Number
            },
            totalAmount : {
                type : Number,
                
            },
            purchasedOn:{
                type: Date,
                default: new Date()
            }
        }
    ]

})

module.exports = mongoose.model("User", userSchema);