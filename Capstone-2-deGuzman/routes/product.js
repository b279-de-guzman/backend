const express = require('express');
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js")

// Create product
router.post("/create", auth.verify, (req, res) =>{
    const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
    productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});


// Retrieve All Active
router.get("/", (req, res) => {
    productController.getAllActive().then(resultFromController => res.send(resultFromController));
})


// Retrieve all products
router.get("/all", auth.verify, (req, res) => {
    const isAdmin = auth.decode(req.headers.authorization).isAdmin
    productController.getAllProducts(isAdmin).then(resultFromController => res.send(resultFromController));
})


// Retrieve product
router.get("/:id", (req, res) => {
    productController.getProduct(req.params.id).then(resultFromController => res.send(resultFromController));
})


// Update product
router.put("/:id", auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin
    productController.updateProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})


// Archive product
router.put("/:id/archive", auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin
    productController.archiveProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})



// Allows us to export the "router" object that will be access in our index.js file
module.exports = router;