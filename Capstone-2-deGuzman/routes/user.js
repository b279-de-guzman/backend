const express = require('express');
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js")


// Create a user
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Login / Authenticate user
router.post("/login", (req,res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Retrieve all users
router.get("/all", (req, res) => {
    userController.getAllUsers(req.body).then(resultFromController => 
        res.send(resultFromController));
       
})


// Retrieve user details
router.get("/:id", (req, res) => {
    userController.getUser(req.params).then(resultFromController => 
        res.send(resultFromController));
})



// Checkout
router.post("/checkout", (req, res) => {

    let data = {
        userId : auth.decode(req.headers.authorization).id,
        isAdmin : auth.decode(req.headers.authorization).isAdmin,
        products : req.body
    }
    userController.checkout(data).then(resultFromController => res.send(resultFromController))
})


// Retrieve user details (blank password)
router.post("/details", auth.verify,  (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    userController.getProfile(userData).then(resultFromController => res.send(resultFromController));
});


// Get user Order (Stretch Goal)
router.post("/orders", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if(userData.isAdmin){
        res.send("Non-admin account required") 
    } else {
        userController.getOrders(userData).then(resultFromController => res.send(resultFromController));
    }
    
});


// Retrieve All Orders (Stretch Goal)
router.post("/allorders",auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    userController.getAllOrders(userData).then(resultFromController => res.send(resultFromController));
});


// Set users to admin (Stretch Goal)
router.put("/:id/setadmin", auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin
    userController.setAdmin(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})

// Add to cart (Stretch Goal)
router.post("/addtocart", auth.verify, async (req, res) => {
    const userId = auth.decode(req.headers.authorization);
    const orders = req.body.orders;

    if (userId.isAdmin) {
        res.send("Admin account cannot purchase.");
    } else {
        const result = await userController.addToCart(userId, orders);
            res.send(result);
    }
})

// Allows us to export the "router" object that will be access in our index.js file
module.exports = router;  