// Find users with letter s in the first name or d in their last name

db.users.find(
	{ $or: [{firstName: {$regex: "s", $options: "i"}}, {lastName: {$regex: "d", $options: "i"}}]},
	{
		firstName: 1,
		lastName: 1,
		_id: 0
	}
);


// Result of querying users who are from HR with their age >= 70

db.users.find(
	{ $and: [{department: "HR"}, {age: {$gte: 70}}]}
);


// Result of querying users with letter e in their first name and age <= 30

db.users.find(
	{ $and: [{firstName: {$regex: "e", $options: "i"}}, {age: {$lte: 30}}]}

);