// Result of aggregation to count the total number of fruits on sale

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$total", fruitsOnSale: {$sum: 1}}},
    {$project: {_id: 0}}
]);

// Result of count the total number of fruits with stock more than 20

db.fruits.aggregate([
	{$match: {stock: {$gte: 20}}},
	{$group: {_id: "$total", enoughStock: {$sum: 1}}},
    {$project: {_id: 0}}
]);

// Result of average price of fruits onSale per supplier

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}},
]);

// Result of highest price of a fruit per supplier

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}},
    {$sort: {_id: -1}}
]);

// Result of lowest price of a fruit per supplier

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}},
    {$sort: {_id: -1}}
]);