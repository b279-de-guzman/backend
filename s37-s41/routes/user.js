const express = require("express");
const router = express.Router();
const userController = require("../controllers/user.js")
const auth = require("../auth.js")


// Route for checking if email already exists.
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => 
		res.send(resultFromController));
})

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => 
		res.send(resultFromController));
})


// Route for user login
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => 
		res.send(resultFromController));
})


// Route for details
router.post("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile(userData.id).then(resultFromController => 
		res.send(resultFromController));
})

// Route for get all info
router.get("/all", (req, res) => {
	userController.getAllUsers(req.body).then(resultFromController => 
		res.send(resultFromController));
})

// Enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		courseId : req.body.courseId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	userController.enroll(data).then(resultFromController => 
		res.send(resultFromController));
})





// Allows us to export the "router" object that will be accessed in our index.js file
module.exports = router;