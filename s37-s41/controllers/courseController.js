const Course = require("../models/Course.js");

// Create a new course
module.exports.addCourse = (data) => {
	
	if(data.isAdmin) {
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		})

		return newCourse.save().then((course, error) => {
			if(error){
				return error;
			}
			return course;
		})
	} 

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
}

// Retrieve all courses function
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}


// Retrieve all active courses function
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}

// Retrieving a specific course

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.id).then(result => {
		return result;
	})
}

// Update a course

module.exports.updateCourse = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
		let updatedCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
		}

		return Course.findByIdAndUpdate(reqParams.id, updatedCourse).then((course, error) => {
			if(error){
				return false;
			} else {
				return "Course Updated";
			}
		})
	} 

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
	
}

module.exports.archiveCourse = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){

		let archivedCourse = {
			isActive : reqBody.isActive
		}
		return Course.findByIdAndUpdate(reqParams.id, archivedCourse).then((course, error) => {
			if(error){
				return false;
			} else {
				return "Course is archived!";
			}
		})
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
}
