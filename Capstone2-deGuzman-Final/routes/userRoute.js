const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js")

// Route For registering User
router.post("/register", (req, res) =>{ 
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})



// Route for login user
router.post("/login", (req,res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})



// Route for specific user details using auth
router.get("/profile", auth.verify, (req,res)=>{
    const userData = auth.decode(req.headers.authorization).id;
    console.log(userData);
    userController.getUser(userData).then(resultFromController=>res.send(resultFromController))
});


// Route for retrieving all admin users
router.get("/admin", (req, res) => {
    userController.getAllAdmin().then(resultFromController => res.send(resultFromController));
})


// Route to check if email exists
router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then( resultFromController => res.send(resultFromController));
});


// Route to retreive all users
router.get("/all", auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;
    userController.getAllUsers(isAdmin).then(resultFromController => res.send(resultFromController));
});


// Route checkout

router.post("/checkout", auth.verify, (req, res,) => {
    let data = {
        isAdmin : auth.decode(req.headers.authorization).isAdmin,
        userId : auth.decode(req.headers.authorization).id,
        product : req.body
    }
        userController.checkout(data).then(resultFromController => res.send(resultFromController));
});



//SG ROUTE FOR GET ALL ORDERS//

router.get("/allOrders",auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin
    userController.getAllOrders(isAdmin).then(resultFromController => res.send(resultFromController))
});


// SG 2 ROUTE FOR GETTING USER's ORDERS
router.get("/myOrders/:userId",auth.verify, (req, res) => {
    let data = {
        product : req.body.orderedProduct
    }
    userController.getOrders(req.params, data).then(resultFromController => res.send(resultFromController))
});






module.exports = router;