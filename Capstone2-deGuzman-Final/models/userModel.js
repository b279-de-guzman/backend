const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First Name is required!"]
	},
	lastName : {
		type : String,
		required : [true, "Last Name is required!"]
	},	
	email : {
		type : String,
		required : [true, "Email is required!"]
	},
	password : {
		type : String,
		required : [true, "Password is required!"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String,
		required : [true, "mobile number is required!"]
	},

	orderedProduct : [

		{
			products : [{
				productId : {
					type : String
				},
				productName : {
					type : String
				},
				quantity : {
					type : Number
				}
			}],
			totalAmount : {
					type : Number
				},
			purchasedOn : {
					type : Date,
					default : new Date()
				}
		}
	]
})

module.exports = mongoose.model("user", userSchema);

