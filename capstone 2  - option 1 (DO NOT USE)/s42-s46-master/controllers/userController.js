const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product.js")


// Check if email already exist

// routes for checking if email already exists.

// Register a user
module.exports.registerUser = (reqBody) => {
    
    return User.findOne({email: reqBody.email}).then(result => {
        /*if(result.email == reqBody.email) {
            return "User already exists! Choose another one."
        } */

        
        let newUser = new User ({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10)
        }) 

        return newUser.save().then((user, error) => {
            if(error) {
                return error;
            } else {
                return "User has been created!";
            }
        })
        
        
})
}

// Login / Authenticate a user
module.exports.loginUser = (reqBody) => {

    return User.findOne({email: reqBody.email}).then(result => {
        if(result == null) {
            return "No user found!"
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if(isPasswordCorrect) {
                return {access: auth.createAccessToken(result)}
            } else {
                return false
            }
        }
    })
}


// Retrieve all users
module.exports.getAllUsers = (reqBody) => {
    return User.find({}).then(result => {
        return result;
    })
}


// Retrieve user details
module.exports.getUser = (reqParams) => {
    return User.findById(reqParams.id).then(result => {
        return result;
    })
}



// Creating order
module.exports.checkout = async (data) => {
    if(!data.isAdmin){
        let productTemp = [];
        let totalAmountTemp = 0;
        let isProductUpdated;
        
        
        data.products.forEach(singleProduct => {
            console.log(singleProduct);
            Product.findById(singleProduct.productId).then(product => { 
                productTemp.push(
                    {
                        productId : singleProduct.productId,
                        productName : product.name,
                        quantity : singleProduct.quantity
                    }
                )
                totalAmountTemp = totalAmountTemp + (product.price * singleProduct.quantity)
            }
            )   
            isProductUpdated = Product.findById(singleProduct.productId).then(product =>{

                product.userOrders.push({userId : data.userId})
                return product.save().then((product, error) => {
                    if(error){
                        return false;
                    } else {
                        return true;
                    }
                })
            })
        });


        let isUserUpdated = await User.findById(data.userId).then(user => {
                user.orderedProduct.push(
                    {

                        product : productTemp,
                        totalAmount : totalAmountTemp
                    }
                );            
                return user.save().then((user, error) => {
                    if(error){
                        return false;
                    }else{
                        return true;
                    }
                })
        })
        

        if(isUserUpdated && isProductUpdated){
            return "Order has been created";
        }else{
            return "Cannot process request. Please try again later!";
        }
    } else {
        return Promise.resolve("Admin account cannot order, please change your account!")
    }
}

// Retrieve user details (blank password)
module.exports.getProfile = (reqBody) => {
    return User.findById(reqBody.id).then(result =>{
        
        if(result){
            result.password = "";
            result.orderedProduct = [];
            return result;
        } else {
            return false
        }
    })
}


// get user order (Stretch Goal)
module.exports.getOrders = (reqBody) => {
    return User.findById(reqBody.id).then(result =>{
        
        if(result){
            return result.orderedProduct;
        } else {
            return false
        }
    })
}


// Retrieve all orders (Stretch Goal)
module.exports.getAllOrders = () => {
    return User.find({}).then(result =>{
        let orders = []
        result.forEach(user => {
            console.log(user.orderedProduct);
            if (user.orderedProduct[0] !== undefined){
                orders.push({
                    userId : user._id, 
                    name : user.firstName + " " + user.lastName,
                    orders : user.orderedProduct
                });
            }
        });
        if(result){
            return orders;
        } else {
            return false
        }
    })
}



// Set a user to admin (Stretch Goal)
module.exports.setAdmin = (reqParams, reqBody, isAdmin) => {
    console.log(isAdmin)
    if(isAdmin){
        let updateUser = {
            isAdmin : reqBody.isAdmin
        }
        
        return User.findByIdAndUpdate(reqParams.id, updateUser).then((User, error) => {
            if(error){
                return false;
            }else{
                return `User Admin privileges set to ${reqBody.isAdmin}`;
            }
        })
    } else {

        return Promise.resolve("Admin account required!")
        
    }
}


// Add to Cart (Stretch Goal)
module.exports.addToCart = async (userId, orders) => {
    let user = await User.findById(userId.id);

    let products = [];
    let totalAmount = 0;

    for (let i = 0; i < orders.length; i++){
        let product = await Product.findById(orders[i].productId);

        const newProduct = {
            productId : product._id,
            productName : product.name,
            price : product.price,
            quantity : orders[i].quantity
        }

        let subTotal = product.price * orders[i].quantity;

        user.orderedProduct.push({
            products : [newProduct],
            purchasedOn : new Date(),
            price : product.price,
            subTotal : subTotal,
            quantity : orders[i].quantity
        })

        totalAmount = totalAmount + subTotal;
        products.push(newProduct);
        product.userOrders.push({userId : userId.id});
        await product.save();
    }

    // Add totalAmount to the last order
    user.orderedProduct[user.orderedProduct.length -1].totalAmount = totalAmount;

    let isUserUpdated = await user.save();

    if (isUserUpdated){
        return "Order added successfully"
    } else {
        return "Cannot process request. Please try again"
    }
}