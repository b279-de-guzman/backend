const Product = require('../models/Product');

// Check if email already exist

// routes for checking if email already exists.

module.exports.addProduct = (data) => {
	if(data.isAdmin) {
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		})

		return newProduct.save().then((product, error) => {
			if(error){
				return error;
			}
			return "Product successfully created!";
		})
	} 

	// If the user is not admin, then return this message as a promise to avoid errors
	return Promise.resolve("Admin access required");
}

module.exports.getAllActive = () => {
    return Product.find({isActive : true}).then(result =>{
        return result;
    })

}

module.exports.getAllProducts = (isAdmin) => {
    if(isAdmin){
        return Product.find({}).then(result =>{
            return result;
        })
    } else {
        return Promise.resolve("Admin access required")
    }
}

module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams).then(result => {
        return result;
    })
}

module.exports.updateProduct = (reqParams, reqBody, isAdmin) => {
    if(isAdmin){
        let updateProduct = {
            name : reqBody.name,
            description : reqBody.description,
            price : reqBody.price
        }
        
        return Product.findByIdAndUpdate(reqParams.id, updateProduct).then((product, error) => {
            if(error){
                return false;
            }else{
                return "Product updated";
            }
        })
    }else{
        return Promise.resolve("Admin access required")
    }
}

module.exports.archiveProduct = (reqParams, reqBody, isAdmin) => {
    console.log(isAdmin)
    if(isAdmin){
        let updateProduct = {
            isActive : reqBody.isActive
        }
        
        return Product.findByIdAndUpdate(reqParams.id, updateProduct).then((course, error) => {
            if(error){
                return false;
            }else{
                return `Product status set to ${reqBody.isActive}`;
            }
        })
    }else{
        return Promise.resolve("Admin access required")
    }
}