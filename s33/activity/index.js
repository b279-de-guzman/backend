// console.log("hello");

// Get all list
fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(json => console.log(json));


// Map method
fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(title => {
	let titleMap = title.map((dataToMap) => dataToMap.title)
	console.log(titleMap);
})

// title();


// Get single list
fetch("https://jsonplaceholder.typicode.com/todos/3")
.then(res => res.json())
.then(json => console.log(json));


// Post to do list
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World",
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));


// Update to do list
fetch("https://jsonplaceholder.typicode.com/todos/3", {
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title : "updated a list",
	    description : "made an update",
	    completed : true,
	    dateCompleted : "May 03, 2023",
	    userId : 3
		})
})
.then(res => res.json())
.then(json => console.log(json));


// Patch list
fetch("https://jsonplaceholder.typicode.com/todos/4", {
	method: "PATCH",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
	    completed : true,
	    dateUpdated : "May 03, 2023",
		})
})
.then(res => res.json())
.then(json => console.log(json));


// Delete a list
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
});